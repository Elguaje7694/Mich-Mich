const express = require('express');
const path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var formidable = require('express-formidable');
var index = require('./routes/index');
var fs = require('fs');


var qr=require('qr-image');
var PDFDocument = require('pdfkit');
var Jimp=require('jimp');

var MongoClient = require('mongodb');
var bcrypt = require('bcrypt-nodejs');
var session = require('express-session');
var multer  = require('multer');

var doc;
var l,p,x,y;
var output="output";
var blabla="A scanner en cas de bug";

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'images')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now()+'.'+mime.extension(file.mimetype))
  }
});
var upload = multer({ storage: storage });

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

var db;
var tabMachines;
var liens=[];
var localMap = new Map();

function getName(callback){
  MongoClient.connect('mongodb://admin:admin@ds125288.mlab.com:25288/michmich', (err, client) => {
    if (err) return console.log(err)
    db = client.db('michmich');

    db.collection("machines").find({"name":{"$exists": true}}).toArray(function(err,result){
      callback(result);
    });
  });
}



getName(function(res){
  tabMachines=res;
  //console.log(tabMachines);
  for(var k in tabMachines){
    //console.log(tabMachines[k].name);
    liens.push("https://qrtestperso.herokuapp.com/#/report/" + tabMachines[k].name);
  }

})

function GenerateQR(array,res){
      doc=new PDFDocument();
      l=0,p=0,x=100,y=15;

      Sequence(array).then(DownloadPdf(res));
}

function Sequence(array){
  return new Promise(function(resolve,reject){

    console.log("dans sequence");
    doc.pipe(fs.createWriteStream(output+'.pdf'));

    for(var j in array){
      console.log("Dans sequence tour j : " + j);
      loadQRImage(array[j],j).then(ChangeToJpg(j)).then(PutInPdf(j));

    }
    doc.end()
    resolve();
  });
}

//Cette fonction va créer un fichier .jgp à partir d'un fichier .png
function ChangeToJpg(j){
  return new Promise(  function(resolve,reject){
    Jimp.read(output+j+".png", function (err, lenna) {
        if (err) throw err;

          lenna.resize(125, 125)            // resize
               .quality(100)                 // set JPEG quality
               .greyscale()                 // set greyscale
               .write(output+j+".jpg");          // save
          console.log("after " + j + " jpg");

    });
    console.log("niquetout");
    resolve();
  });

}

//Cette fonction va créer une Promise qui est "resolve" lorsque le qr code est générer et sauver dans un fichier .png
function loadQRImage(name,j){
  return new Promise(  function(resolve,reject){
    var url="https://qrtestperso.herokuapp.com/#/report/"+name;
    console.log(url);
    var code = qr.image(url,{type : 'png'});
    code.pipe(fs.createWriteStream(output+j+'.png',{flags:'w'}));
    resolve();
  });
}

//Cette fonction positionne l'image .jpg en cours dans le pdf.
function PutInPdf(j){
  console.log("wouohou");
  return new Promise(  function(resolve,reject){
 console.log("Start in pdf");
 if(p>0 && p%4==0){
   x=370;
   y=15;
 }
 if(p>0 && p%8==0){
   x=100;
   y=15;
   doc.addPage();
 }
 console.log("asking for " + output + j+".jpg");
 doc.image('./'+output+j+'.jpg',x,y).text(blabla,x,y+115);

 y+=150;
 p++;
 resolve();
});
}


//Cette fonction va download le fichier pdf àpres 1s.
function DownloadPdf(res){
  return new Promise(  function(resolve,reject){
  var ret;
/*  setTimeout(function(){
      ret = "hello";
      var file = path.join(__dirname, 'output.pdf');
      res.download(file, function (err) {
         if (err) {
             console.log("Error");
             console.log(err);
         } else {
             console.log("Success");
         }
       });
  },1000);*/
  resolve();
});
}


//Cette fonction va supprimer tous les fichiers caches qui ont du être créer pour former le pdf. (Les fichiers .png et .jpg seront supprimer)
function DelCache(array){
  console.log("in cache");
  for(var k=0;k<array.length;k++){
    console.log(k);
        fs.unlink('./output'+k+'.png',function(err){
             if(err) return console.log(err);
        });


        fs.unlink('./output'+k+'.jpg',function(err){
             if(err) return console.log(err);
        });
  }
}


app.use(express.static(path.join(__dirname, 'client/build')));
app.use(express.static(path.join(__dirname, '/')));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(formidable());
app.use('/', index);

function insertComputer(computer){
  console.log("insertComputer");
  MongoClient.connect('mongodb://admin:admin@ds125288.mlab.com:25288/michmich', (err, client) => {
    if (err) return console.log(err)
    db = client.db('michmich');
    existsInDb(computer)
      .then(function(){// si le pc n'est pas en db on l'insert
        console.log("insert ok");
        db.collection("machines").insert(computer);
      })
      .catch(function(){
        //console.log("déja présent");
      });
  })
}

function arrayCleaner(dirtyArray){
  var cleanArray = [];
  for(var i=4; i<dirtyArray.length; i++){
    var res = dirtyArray[i].replace(/\"/g,"$");// je remplace les "" par le char '$' pour les distinguer d'une ligne vide
    if(res !== ""){// je supprime les lignes vides
      var clean = res.replace(/\$/g, ""); // je supprime les caractères d'aide
      cleanArray.push(clean);
    }
  }
  return cleanArray;
}

app.post('/getbug', (req, res) => {
  db.collection('machines').find({}).toArray(function (error, results) {
    res.send({machines: results});
  });
});

app.post('/qr', (req, res) => {
  var local = req.files.file.name.substring(6,9); // numéro de la classe
  var content = fs.readFileSync(req.files.file.path, 'utf8');// fichier ipscan qui provient du front
  var tokens = content.split(/;|\n|\r/);// tableau contenant les valeurs des fichiers pas encore néttoyées
  var cleanArray = arrayCleaner(tokens);// nettoie le tableau
  for(var i=0; i<cleanArray.length; i++){
    if(i % 4 == 0){// à chaque ligne on crée un nouvel ordinateur
      var computer = {};// objet qui contient les infos sur la machine à mettre en db
      computer.local = local;
      if(!localMap.has(local))localMap.set(local, new Set());// si le local est un nouveau local on crée un set qui va contenir les noms des
                                                            // machines de ce local


      computer.statut = "Active";
      j=0;
    }
    switch (i%4) {// l'ordinateur est rempli avec valeurs
      case 0: computer.ip = cleanArray[i];
        break;
      case 1 :
              computer.name = cleanArray[i];
              localMap.get(local).add(cleanArray[i]);//on ajoute le nom de la machine au set de son local
        break;
      case 2: computer.mac = cleanArray[i];
        break;
      case 3: computer.comment = cleanArray[i];
      default:
        break;
    }
    j++;
    if(j == 4){
      insertComputer(computer);// une fois l'ordinateur créé on l'insert en db
      console.log("computer créé");
    }
  }
  var table=[];
 localMap.forEach(function(key, elem){
    console.log("key");
    console.log(key);
    console.log("elem");
    console.log(elem);
    console.log(Array.from(key));
    table=table.concat(Array.from(key));
  });
  console.log("added");
  console.log(table);
  GenerateQR(table,null);

  setInactive();// on désactive les ordinateurs qui doivent l'être
  res.send({ testurl: 200 });
});

function setInactive(){
    MongoClient.connect('mongodb://admin:admin@ds125288.mlab.com:25288/michmich', (err, client) => {
      if(err)return console.log(err);
      db = client.db("michmich");
      db.collection("machines").find({}).forEach(function(res){
        if(localMap.get(res.local) != undefined){ // si la map ne connaît pas le local auquel appartient le pc on ne fait rien
          if(!localMap.get(res.local).has(res.name)){// si le pc est en db mais pas dans le fichier on le désactive
            db.collection("machines").update(
              {"name" : res.name},
              {$set : {"statut" : "Inactive"}}
            );
          }
        }
      });
    });
}

function existsInDb(computer){
  console.log("existsInDb")
  return new Promise(function(resolve, reject){
    MongoClient.connect('mongodb://admin:admin@ds125288.mlab.com:25288/michmich', (err, client) => {
      if (err) return console.log(err)
      db = client.db('michmich');
      db.collection("machines").findOne({"name":computer.name}, function(err, res){
        if(res == null)resolve();
        else{
          reject();
        }
      });
    })
  });

}

app.get('/report/:nummachine', (req, res) => {
  res.send({express: req.params.nummachine})
});

app.post('/report', (req, res) => {
  var filepath="";
  console.log(req.files.photo);
  if(typeof req.files.photo !== 'undefined'){
    fs.createReadStream(req.files.photo.path).pipe(fs.createWriteStream('./public/photos/'+req.files.photo.name));
    filepath = '/public/photos/'+req.files.photo.name;
  }
  
  var currentDate = new Date();
  var day = currentDate.getDate();
  var month = currentDate.getMonth() + 1;
  var year = currentDate.getFullYear();
  var today = day+'/'+month+'/'+year;
  db.collection('machines').find({"name":req.fields.machine}).limit(1).next(function(err,result){

    if(result!=null){

      db.collection('machines').update({name : req.fields.machine},
        { $push: { rapports: {id:Date.now(), 
                              user : req.fields.email, date : today,
                              description : req.fields.description,
                              photo : filepath,
                              resolved : false
                            } } }, function(err, added) {
      if( err || !added ) {
        console.log("Erreur");
      }
      else {
        console.log("Rapport de bug ajouté: ");
        res.send("Voila");
      }
    });

  }});
  //var file=req.files.photo.path;
  
});

app.post('/downloadqrcode',(req,res) => {
  console.log(req.fields.machines);
  //console.log(req.body.machines);
  var renvoir = req.fields.machines.split(/,/);
  GenerateQR(renvoir,res);
res.send({testurl : 200});

});


  //var file=req.files.photo.path;



app.post('/login',(req,res) =>{
  var user="";
  var password = bcrypt.hashSync("a", null);
  var json = JSON.parse(req.fields.json);
  db.collection('admins').find({"admin" : json.user}).limit(1).next(function(err,result){
    if(err)throw err;
    if(result!=null){
      user=result.admin;
      password=result.password;
    }
    if(bcrypt.compareSync(json.password,password)){
       console.log("Login success");
       res.send({testurl : 200})
    }else{
      console.log("Login failed");
    }
  });
})


app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname+'/client/build/index.html'));
});


const port = process.env.PORT || 5000;
app.listen(port);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});




module.exports = app;
