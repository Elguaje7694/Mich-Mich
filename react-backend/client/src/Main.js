import React, { Component } from "react";
import { Route, NavLink, HashRouter } from 'react-router-dom';
import Login from './Login';
import Report from './Report';
import Users from './Users';
import Felix from './Felix';
import FileReader from './FileReader';
import './index.css';

class Main extends Component {
  render() {
    return (
        <HashRouter>
            <div>
            <h1>QR Bug Pro Master</h1>
            <ul className="header">
                <li><NavLink exact to="/">Login</NavLink></li>
                <li><NavLink to="/report">Report</NavLink></li>
                <li><NavLink to="/users">Users</NavLink></li>
                <li><NavLink to="/felix">Felix</NavLink></li>
                <li><NavLink to="/fileReader">Injection</NavLink></li>
            </ul>
            <div className="content">
                    <Route exact path="/" component={Login}/>
                    <Route path="/report" component={Report}/>
                    <Route path="/users" component={Users}/>
                    <Route path="/felix" component={Felix}/>
                    <Route path="/fileReader" component={FileReader}/>
            </div>
            </div>
        </HashRouter>
    );
  }
}

export default Main;
