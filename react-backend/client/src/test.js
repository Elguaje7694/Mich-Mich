import React, { Component } from 'react';
import './App.css';
var FileInput = require("react-file-input");

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      url: '',
      responseGet: '',
      responsePost: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.callApi()
      .then(res => this.setState({ responseGet: res.express }))
      .catch(err => console.log(err));
  }

  callApi = async () => {
    const response = await fetch("/hello");
    const body = await response.json();

    if (response.status !== 200) throw Error(body.message);

    return body;
  };

  testQr = async () => {
    const response = await fetch('/qr', {
                        method: 'POST',
                        headers: {
                          'Accept': 'application/json',
                          'Content-Type': 'application/json',
                        },
                        body:JSON.stringify({url:this.state.url}),
                      });
    const body = await response.json();

    if (response.status !== 200) throw Error(body.message);

    return body;
  };

  handleChange(event) {
    this.setState({url: event.target.value});
  }

  handleSubmit(event) {
    console.log('An url was submitted: ' + this.state.url);
    this.testQr()
      .then(res => this.setState({ responsePost: res.testurl }))
      .catch(err =>console.log(err))
    event.preventDefault();
  }

  render() {
    return (
      <div>
        <p>test GET : {this.state.responseGet}</p>
        <p>test POST : {this.state.responsePost}</p>
        <form onSubmit={this.handleSubmit}>
          <label>
            Name:
            <input type="text" value={this.state.url} onChange={this.handleChange} />
          </label>
          <input type="submit" value="Submit" />
        </form>
      </div>
    );
  }
}

export default App;
