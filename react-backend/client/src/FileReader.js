import React, {Component} from 'react';

class FileReader extends Component {
  constructor(props) {
      super(props);
      this.state ={
        file:null
      }
      this.onFormSubmit = this.onFormSubmit.bind(this)
      this.onChange = this.onChange.bind(this)
      this.fileUpload = this.fileUpload.bind(this)
  }

  onFormSubmit(e){
    e.preventDefault() // Stop form submit
    this.fileUpload(this.state.file).then((response)=>{
      console.log(response.data);
    })
  }

  onChange(e) {
    this.setState({file:e.target.files[0]})
  }

  fileUpload = async (file) => {
    const formData = new FormData();
    console.log("file : ", file);
    formData.append('file',file);
    const response = await fetch ('/qr', {
      method: 'POST',
      body:formData,
    });
    const body = await response;

    if (response.status !== 200) throw Error(body.message);

    return body;
  }

  render() {
    return (
      <form onSubmit={this.onFormSubmit}>
        <h1>File Upload</h1>
        <input type="file" onChange={this.onChange} />
        <button type="submit">Upload</button>
      </form>
   )
  }
}
export default App;
