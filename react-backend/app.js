var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var formidable = require('express-formidable');
var index = require('./routes/index');
var users = require('./routes/users');
var fs = require("fs");
const MongoClient = require('mongodb');
var app = express();

//map des classes de l'ipl
var localMap = new Map();
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(formidable());
app.use('/', index);
app.use('/users', users);

app.get('/hello', (req, res) => {
  res.send({ express: 'Hello From Express' });
});

app.post('/qr', (req, res) => {
  var local = req.files.file.name.substring(6,9); // numéro de la classe
  var content = fs.readFileSync(req.files.file.path, 'utf8');// fichier ipscan qui provient du front
  var tokens = content.split(/;|\n|\r/);// tableau contenant les valeurs des fichiers pas encore néttoyées
  var cleanArray = arrayCleaner(tokens);// nettoie le tableau

  for(var i=0; i<cleanArray.length; i++){
    if(i % 4 == 0){// à chaque ligne on crée un nouvel ordinateur
      var computer = {};// objet qui contient les infos sur la machine à mettre en db
      computer.local = local;
      if(!localMap.has(local))localMap.set(local, new Set());// si le local est un nouveau local on crée un set qui va contenir les noms des
                                                            // machines de ce local
      computer.statut = "Active";
      j=0;
    }
    switch (i%4) {// l'ordinateur est rempli avec valeurs
      case 0: computer.ip = cleanArray[i];
        break;
      case 1 :
              computer.name = cleanArray[i];
              localMap.get(local).add(cleanArray[i]);//on ajoute le nom de la machine au set de son local
        break;
      case 2: computer.mac = cleanArray[i];
        break;
      case 3: computer.comment = cleanArray[i];
      default:
        break;
    }
    j++;
    if(j == 4)insertComputer(computer);// une fois l'ordinateur créé on l'insert en db
  }
  setInactive();// on désactive les ordinateurs qui doivent l'être
  res.send({ testurl: 200 });
});

function arrayCleaner(dirtyArray){
  var cleanArray = [];
  for(var i=4; i<dirtyArray.length; i++){
    var res = dirtyArray[i].replace(/\"/g,"$");// je remplace les "" par le char '$' pour les distinguer d'une ligne vide
    if(res !== ""){// je supprime les lignes vides
      var clean = res.replace(/\$/g, ""); // je supprime les caractères d'aide
      cleanArray.push(clean);
    }
  }
  return cleanArray;
}

function setInactive(){
    MongoClient.connect('mongodb://admin:admin@ds125288.mlab.com:25288/michmich', (err, client) => {
      if(err)return console.log(err);
      db = client.db("michmich");
      db.collection("machinesTest").find({}).forEach(function(res){
        if(localMap.get(res.local) != undefined){ // si la map ne connaît pas le local auquel appartient le pc on ne fait rien
          if(!localMap.get(res.local).has(res.name)){// si le pc est en db mais pas dans le fichier on le désactive
            db.collection("machinesTest").update(
              {"name" : res.name},
              {$set : {"statut" : "Inactive"}}
            );
          }
        }
      });
    });
}

function existsInDb(computer){
  return new Promise(function(resolve, reject){
    MongoClient.connect('mongodb://admin:admin@ds125288.mlab.com:25288/michmich', (err, client) => {
      if (err) return console.log(err)
      db = client.db('michmich');
      db.collection("machinesTest").findOne({"name":computer.name}, function(err, res){
        if(res == null)resolve();
        else{
          reject();
        }
      });
    })
  });

}

function insertComputer(computer){
  MongoClient.connect('mongodb://admin:admin@ds125288.mlab.com:25288/michmich', (err, client) => {
    if (err) return console.log(err)
    db = client.db('michmich');
    existsInDb(computer)
      .then(function(){// si le pc n'est pas en db on l'insert
        db.collection("machinesTest").insert(computer);
      })
      .catch(function(){
        //console.log("déja présent");
      });
  })
}

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
