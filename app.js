var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var formidable = require('express-formidable');
var index = require('./routes/index');
var users = require('./routes/users');
var fs = require('fs');


var MongoClient = require('mongodb').MongoClient;
var bcrypt = require('bcrypt-nodejs');
var session = require('express-session');
var multer  = require('multer');
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'images')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now()+'.'+mime.extension(file.mimetype))
  }
});
var upload = multer({ storage: storage });

var app = express();


var db;

MongoClient.connect('mongodb://admin:admin@ds125288.mlab.com:25288/michmich', (err, client) => {
  if (err) return console.log(err)
  db = client.db('michmich')
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});




// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(formidable());
app.use('/', index);
app.use('/users', users);




app.get('/hello', (req, res) => {
  res.send({ express: 'Hello From Express' });
});

app.post('/report/:nummachine', (req, res) => {
  db.collection('machines').save(req.body, (err, result) => {
    if (err) return console.log(err)

    console.log('saved to database')
    res.redirect('/')
  })
});


app.post('/report',upload.single('photo'), (req, res) => {
  
  console.log(req.files);
  var currentDate = new Date();
  var day = currentDate.getDate();
  var month = currentDate.getMonth() + 1;
  var year = currentDate.getFullYear();
  var today = day+'/'+month+'/'+year;
  var filename=req.files.photo.name;
  
  console.log(filename);
  db.collection('machines').update({machine : req.fields.machine}, 
      { $push: { rapports: {user : req.fields.email, date : today, 
                            description : req.fields.description,
                            photo : filename} } }, function(err, added) {
    if( err || !added ) {
      console.log("Erreur");
    }
    else {
      console.log("Rapport de bug ajouté: ");
      }
  });
});

app.post('/login',(req,res) =>{
  console.log(req.body.user);
  var user="";
  var password="";
  db.collection('admins').find({"admin" : req.body.user}).limit(1).next(function(err,result){
    if(result!=null){
      
      user=result.admin;
      password=result.password;
    }
    console.log(bcrypt.hashSync(req.body.password));
    if(bcrypt.compareSync(req.body.password,password)){
      console.log("Login success");
    }
    
    
  });

    

})



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
