import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import './Login.css';
import Cookies from 'universal-cookie';
import ReactDOM from 'react-dom';
import Main from './Main.js';
import Report from './Report.js';

const cookies = new Cookies();

 class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: "",
      password: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount(){
    if(cookies.get('isAdmin') != undefined){
      ReactDOM.render(<Main />, document.getElementById('root'));
    }
  }

  validateForm() {
    return this.state.user.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = event => {
    this.sendData()
      .catch(err =>console.log(err));
    event.preventDefault();
  }

  report = event =>{
    ReactDOM.render(<Report/>, document.getElementById('root'));
  }

  sendData = async () => {
    var data = new FormData();
    data.append("json", JSON.stringify({user:this.state.user,password:this.state.password}));
    const response = await fetch('/login', {
                        method: 'POST',
                        body:data
                      });
    const body = await response.json();

    if (response.status !== 200) throw Error(body.message);
    else{
      console.log("ADMIN OK");
      document.cookie = "admin=true";
      //cookies.set('isAdmin', true);
      ReactDOM.render(<Main />, document.getElementById('root'));
    }
    return body;
  };

  render() {
    return (
      <div className="Login">
        <form onSubmit={this.report}>
        <Button
        block
        bsSize="large"
        type="submit"
        >
        Rapporter un problème
        </Button>
        </form>
        <p></p>
        <h2>Login</h2>
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="user" bsSize="large">
            <ControlLabel>Username   </ControlLabel>
            <FormControl
              autoFocus
              type="user"
              value={this.state.user}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <ControlLabel>Password   </ControlLabel>
            <FormControl
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
          </FormGroup>
          <Button
            block
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
          >
            Login
          </Button>
        </form>
      </div>
    );
  }
}

export default Login;
