import React, { Component } from 'react';
import './Admin.css';
import ReactTable from 'react-table'
import matchSorter from 'match-sorter'
import Bugs from './Bugs'
import Machines from './Machines'
import 'react-table/react-table.css'
import {Button} from 'react-bootstrap'
import Cookies from 'universal-cookie';
import ReactDOM from 'react-dom';
import Login from './Login.js';

const cookies = new Cookies();

class Admin extends Component {
    constructor(props) {
        super(props);

        this.state = {
            page: "machines"
        };
    }
    componentDidMount(){
      // if(cookies.get('isAdmin') == undefined){
      if(document.cookie.admin == true){
        ReactDOM.render(<Login/>, document.getElementById('login'));
      }
    }
    switch = () => {
        switch (this.state.page) {
            case "bugs":
            this.setState({ page: 'machines' });
                break;

            default:
            this.setState({ page: 'bugs' });
                break;
        }
    }

    render() {
        switch (this.state.page) {
            case "bugs":
            return(
                <div>
                <Button bsStyle='primary' onClick={(e) => this.switch()}>Machines</Button>
                <Bugs/>
                </div>
            );
                break;

            default:
                return(
                    <div>
                       <Button bsStyle='primary' onClick={(e) => this.switch()}>Bugs</Button>
                    <Machines/>
        </div>
                );
                break;
        }
    }
}

export default Admin;
