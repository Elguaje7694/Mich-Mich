import React, { Component } from "react";
import { Route, NavLink, HashRouter } from 'react-router-dom';
import Login from './Login';
import Report from './Report';
import Machines from './Machines';
import Bugs from './Bugs';
import FileReader from './FileReader';
import './index.css';

class Main extends Component {


  render() {
    return (
        <HashRouter>
            <div>
            <h1>QR Bug Pro Master</h1>
            <ul className="header">
                <li><NavLink exact to="/">Machines</NavLink></li>
                <li><NavLink to="/rapports">Rapports</NavLink></li>
                <li><NavLink to="/report">Faire un rapport</NavLink></li>
                <li><NavLink to="/fileReader">Ajout de Machine</NavLink></li>
            </ul>
            <div className="content">
                    <Route exact path="/" component={Machines}/>
                    <Route path="/rapports" component={Bugs}/>
                    <Route path="/report" component={Report}/>
                    <Route path="/fileReader" component={FileReader}/>
            </div>
            </div>
        </HashRouter>
    );
  }
}

export default Main;
