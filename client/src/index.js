import React from 'react';
import ReactDOM from 'react-dom';
import Main from './Main';
import './index.css';
import Login from './Login';
import Report from './Report'
import Cookies from 'universal-cookie';
import bootstrap from 'bootstrap/dist/css/bootstrap.css';

const cookies = new Cookies();

// if(cookies.get("admin") !== undefined){
if(document.cookie === 'admin=true'){
  ReactDOM.render(<Main/>, document.getElementById('root'));
}else if(window.location.href.includes('report')){
  ReactDOM.render(<Report/>, document.getElementById('root'));
}else{
  ReactDOM.render(<Login/>, document.getElementById('root'));
}
