import React, { Component } from 'react';
import ReactTable from 'react-table'
import matchSorter from 'match-sorter'
import 'react-table/react-table.css'
import {Button} from 'react-bootstrap'

class Bugs extends Component {
    constructor(props) {
        super(props);

        this.state = {
            machines: [],
            checkedRow: []
        };
    }
    componentDidMount() {
        this.getBugs().then(res => this.setState({ machines: res.machines }));
      }

      getBugs = async () => {
        const response = await fetch('/getbug', {
                            method: 'POST'
                          });
        const body = await response.json();
    
        if (response.status !== 200) throw Error(body.message);
        return body;
      };

  render() {
    console.log(this.state.machines);
    console.log("length " + this.state.machines.length);
    const machines = this.state.machines;
    var reports = [];
    machines.map(function (elem, key){
        if(elem.rapports != undefined){
            for(var i= 0; i < elem.rapports.length; i++)
            {
                var cpy = JSON.parse(JSON.stringify(elem.rapports));
                cpy[i].name = elem.name;
                reports.push(cpy[i]);
                console.log(cpy[i]);
            }
        }
    });

    const columns = [{
        Header: 'Nom d\'utilisateur',
        accessor: 'user'
    }, {
        Header: 'Date',
        accessor: 'date'
    }, {
        Header: 'Description',
        accessor: 'description'
    },{
        Header: 'Nom de la machine',
        accessor: 'name'
    }]

    return(
        <div>
            <div style={{textAlign: 'center'}}>
                <h1>Page des rapports de bugs</h1>      
            </div>
            <br />
            <br />
    <ReactTable
        data={reports}
        columns={columns}
        className="-striped -highlight"
        SubComponent={row => {
            if(row.original.photo !== ""){
                var link = "http://localhost:5000" + row.original.photo;
            }
            const subcolumns = [{
                Header: 'Description',
                accessor: 'description'
            }, {
                Header: 'Image',
                id: 'image',
                Cell: ({ row }) => (<div style={{textAlign: 'center'}}>
                <img style={{width:200}} src={link}/></div>)
            }]
            return (
              <div style={{ padding: "20px" }}>
                <ReactTable
                  data={[row.original]}
                  columns={subcolumns}
                  minRows={1}
                  showPagination={false}
                />
              </div>
            );
        }}

    /></div>
    );    
  }
}

export default Bugs;