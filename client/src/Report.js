import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import './Report.css';
import Cookies from 'universal-cookie';
import ReactDOM from 'react-dom';

 class Report extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      machine: "",
      description:"",
      photo:"",
      responsePost:""
    };
  }

  componentDidMount() {
    this.callApi()
    .then (res => this.setState({machine:res.express}))
    .catch(err => console.log(err));
  };

  callApi = async () => {
    var urlTab = window.location.href.substring(43);
    //var valeur = urlTab[urlTab.length-1];
    const response = await fetch("/report/" + urlTab);
    const body = await response.json();

    if (response.status !== 200) throw Error(body.message);

    return body;
  };

  validateForm() {
    return this.state.email.length > 0 && this.state.machine.length > 0 && this.state.description.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }
  onChange = event => {
    this.setState({photo:event.target.files[0]});
  }


  handleSubmit = event => {
    this.sendData()
      .then(res => this.setState({ responsePost: "Le rapport a été envoyé" }))
      .catch(err =>console.log('Erreur!!!! : '+err));
    event.preventDefault();
  }

  sendData = async () => {
    const formData = new FormData();
    formData.append('email',this.state.email);

    formData.append('machine',this.state.machine);
    console.log(formData.get('machine'));
    formData.append('description',this.state.description);
    console.log(formData.get('description'));
    console.log(this.state.photo);

    formData.append('photo',this.state.photo);
    const response = await fetch ('/report', {
      method: 'POST',
      body:formData,
    });
    const body = await response;


    if (response.status !== 200) throw Error(body.message);

    return body;
  };

  render() {
    return (
      <div className="Report">


        <h2>Rapporter un problème</h2>


        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="email" bsSize="large" encType="multipart/form-data">
            <ControlLabel>email  :  </ControlLabel>
            <FormControl
              autoFocus
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="machine" bsSize="large">
            <ControlLabel>machine  :  </ControlLabel>
            <FormControl
              value={this.state.machine}
              onChange={this.handleChange}
              type="machine"
            />
          </FormGroup>
          <FormGroup controlId="description" bsSize="large">
            <ControlLabel>description  :  </ControlLabel>
            <FormControl
              value={this.state.description}
              onChange={this.handleChange}
              type="description"
            />
          </FormGroup>
          <FormGroup controlId="photo" bsSize="large">
            <ControlLabel>photo  :  </ControlLabel>
            <input type="file" onChange={this.onChange} />

          </FormGroup>
          <Button
            block
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
          >
            Report
          </Button>
          <p></p>
          <p id='confirm'>{this.state.responsePost}</p>
        </form>
      </div>
    );
  }
}

export default Report;
