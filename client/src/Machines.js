import React, { Component } from 'react';
import ReactTable from 'react-table'
import matchSorter from 'match-sorter'
import 'react-table/react-table.css'
import {Button} from 'react-bootstrap'

class Machines extends Component {
    constructor(props) {
        super(props);

        this.state = {
            machines: [],
            checkedRow: []
        };
    }
    componentDidMount() {
        this.getBugs().then(res => this.setState({ machines: res.machines }));
      }

      getBugs = async () => {
        const response = await fetch('/getbug', {
                            method: 'POST'
                          });
        const body = await response.json();

        if (response.status !== 200) throw Error(body.message);
        return body;
      };

      /*resolve = async (row) => {
        const formData = new FormData();
        formData.append('id',row.id);
        formData.append('machine',row.name);
        console.log(row);
        const response = await fetch('/resolve', {
                            method: 'POST',
                            body:formData,
                          });
        const body = await response.json();
        if (response.status !== 200) throw Error(body.message);
        return body;
      };*/

      downloadQR = async () => {
        console.log(this.state.checkedRow);
        const formData = new FormData();
        formData.append('machines',this.state.checkedRow);
        console.log(formData.get('machines'));

        const response = await fetch('/downloadqrcode', {
                            method: 'POST',
                            /*headers:{
                              'Accept' : 'application/json',
                              'Content-Type' : 'application/json',
                            },*/
                            body:formData,
                          });
        const body = await response.json();
        if (response.status !== 200) throw Error(body.message);
        return body;
      };

      checkQr = (row) => {
        if(this.state.checkedRow.includes(row.name)){
            console.log("remove");
            var arrayvar = this.state.checkedRow.slice();
            arrayvar.splice(arrayvar.indexOf(row.name), 1);
            this.setState({ checkedRow: arrayvar });
            console.log(this.state.checkedRow);
        }else{
            console.log("add");
            this.setState(prevState => ({
                checkedRow: [...prevState.checkedRow, row.name]
              }));
            console.log(this.state.checkedRow);
       }
      };

  render() {
    console.log(this.state.machines);
    console.log("length " + this.state.machines.length);
    const machines = this.state.machines;
    
    const columns = [
        {
            Header: "Machine description", 
            columns: [
                {
                    Header: 'Nom de machine',
                    accessor: 'name'
                }, {
                    Header: 'Local',
                    accessor: 'local'
                }, {
                    Header: 'Adresse Ip',
                    accessor: 'ip'
                }, {
                    Header: 'Adresse Mac',
                    accessor: 'mac'
                }
            ]
        },{ 
            Header: ({ row }) => (<div style={{textAlign: 'center'}}><Button bsStyle='primary' onClick={(e) => this.downloadQR().then(function(){
                window.location.replace("./output.pdf");
              })}>Download</Button></div>), 
            columns: [
                {
                    Header: 'Code QR',
                    id: 'downloadQR',
                    Cell: ({ row }) => (<div style={{textAlign: 'center'}}><input onChange={(e) => this.checkQr(row)} id="checkBox" type="checkbox"/></div>)
                }
            ]
        }
    ]

    const subcolumns = [{
        show: false,
        accessor: 'name'
    },{
        show: false,
        accessor: 'id'
    },{
        Header: 'Nom d\'utilisateur',
        accessor: 'user'
    }, {
        Header: 'Date',
        accessor: 'date'
    }, {
        Header: 'Description',
        accessor: 'description'
    }]

    return(
        <div>
            <div style={{textAlign: 'center'}}>
                <h1>Page de la liste des machines</h1>
            </div>
                <br />
                <br />
    <ReactTable
        data={machines}
        columns={columns}
        className="-striped -highlight"
        SubComponent={row => {
            if(row.original.rapports !== undefined){
                row.original.rapports.forEach(element => {
                    element.name = row.original.name;
                });
            }
            console.log(row);
            return (
              <div style={{ padding: "20px" }}>
                <em>
                  Liste des Rapports de bug pour cette machine
                </em>
                <br />
                <br />
                <ReactTable
                  data={row.original.rapports}
                  columns={subcolumns}
                  minRows={1}
                  showPagination={false}
                />
              </div>
            );
        }}

    /></div>
    );
  }
}

export default Machines;
