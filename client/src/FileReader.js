import React, {Component} from 'react';
import Cookies from 'universal-cookie';
import ReactDOM from 'react-dom';
import Login from './Login.js';
import {Button} from 'react-bootstrap';

const cookies = new Cookies();

class FileReader extends Component {
  constructor(props) {
      super(props);
      this.state ={
        file:null
      }
      this.onFormSubmit = this.onFormSubmit.bind(this)
      this.onChange = this.onChange.bind(this)
      this.fileUpload = this.fileUpload.bind(this)
  }

  onFormSubmit(e){
    e.preventDefault() // Stop form submit
    this.fileUpload(this.state.file).then(function(){
      window.location.replace("./output.pdf");
    });
  }

  componentDidMount(){
    if(document.cookie.admin == true){
         ReactDOM.render(<Login/>, document.getElementById('login'));
    }
    // if(cookies.get('isAdmin') == undefined){
    // }
  }

  onChange(e) {
    this.setState({file:e.target.files[0]})
  }

  fileUpload = async (file) => {
    const formData = new FormData();
    console.log("file : ", file);
    formData.append('file',file);
    const response = await fetch ('/qr', {
      method: 'POST',
      body:formData,
    });
    const body = await response;

    if (response.status !== 200) throw Error(body.message);

    return body;
  }

  render() {
    return (
      <div style={{textAlign: 'center'}}>
        <h1>Upload du fichier IPScan</h1> 
        <br />
        <br />
        <form onSubmit={this.onFormSubmit}>
          <input type="file" onChange={this.onChange} />
          <br />
          <br />
          <Button type="submit">Upload</Button>
        </form>      
      </div>
   )
  }
}
export default FileReader;
